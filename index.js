$(document).ready(function () {

    function getQuery(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.href.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2])
        }
        return null
    }

    function isiOS() {
        if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
            return 1;
        } else {
            return 0;
        }
    }

    function init() {
        var id = location.hash.substr(1),
            host = "http://zhan.qianlong.com";
        if (id) {
            var a = $.ajax({
                url: "http://zhan.qianlong.com/weixin/painter_get?key=tuye_painter&id=" + id,
                type: "get",
                async: false,
                dataType: "json"
            });
            a.done(function (data) {
                var result = JSON.parse(data.resultmsg);
                $('.pic-sample').attr('src', host + result[0].image);
            });
        } else {
            $('.pic-sample').attr('src', 'assets/pic_sample.png');
        }

        if (isiOS()) {
            $('.dialog li:nth-child(odd) p').css({
                'margin-right': '2em'
            });
        }
        $('.dialog li p').before('<span></span>');
        $('.dialog li span').each(function () {
            var r = Math.floor(Math.random() * 4) * 33;
            $(this).css({
                'background-position-y': r + '%'
            });
        });

        $('audio')[0].play();
    }

    function endAnime() {
        $('.more').animate({
            opacity: 1
        }, 1500);
    }

    function mainAnime() {
        window.scrollTo(0, $('.main').height());
        $('.start').children().each(function (index, element) {
            $(element).delay(index * 2000).animate({
                opacity: 1
            }, 1500);
        });
        $('.dialog').delay(10 * 1000).animate({
            opacity: 1
        }, 1500, function () {
            $('html').css('overflow-y', 'auto');
            $('.pic-arrow').delay(6000).fadeOut();
        });
        var t = setInterval(function () {
            if ($('body').scrollTop() === 0) {
                endAnime();
                clearInterval(t);
            }
        }, 1000);
    }

    function bind() {
        $('.dialog li').click(function () {
            $(this).find('img').animate({
                width: '100%'
            }, 1000);
            $(this).siblings().find('img').animate({
                width: '0'
            }, 1000);
        });
        $('.main .pic-qt').click(function () {
            $('.main').fadeOut();
            $('.paint').fadeIn();
        });
        $('.paint .pic-qt').click(function () {
            $('.main').fadeOut();
            $('.paint').fadeIn();
        });
        $('.btn-start').click(function () {
            $('.pic-sample').fadeOut();
            $('.btn-start').fadeOut();
            $('.btn-show').fadeOut();
            $('.paint .pic-qt').fadeOut();
            $('.btn-paint').fadeIn();
            $('canvas').fadeIn();
        });
        $('.btn-back').click(function () {
            $('.pic-sample').fadeIn();
            $('.btn-start').fadeIn();
            $('.btn-show').fadeIn();
            $('.paint .pic-qt').fadeIn();
            $('.btn-paint').fadeOut();
            $('canvas').fadeOut();
        });
        $('.btn-show').click(function () {
            $('.paint').fadeOut();
            $('.main').fadeIn(mainAnime);
        });
    }

    init();
    bind();

    $('.paint').fadeIn();

});
