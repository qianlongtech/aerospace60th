function BrushDraw(brush_deltx, brush_delty) {
    var b = this;
    this.canvas = document.getElementById('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.ctx.lineJoin = 'round';
    this.brush_width = '8';
    this.brush_color = '#fff';
    this.ctx.lineWidth = b.brush_width;
    this.ctx.fillStyle = b.brush_color;
    this.ctx.strokeStyle = b.brush_color;
    this.flag = false;
    this.p = {
        x: 0,
        y: 0
    };
    this.cx = 0;
    this.cy = 0;
    this.step = [];

    this.isTouch = function (event) {
        var type = event.type;
        if (type.indexOf('touch') >= 0) {
            return true;
        } else {
            return false;
        }
    };

    this.pos = function (event) {
        var x, y;
        if (b.isTouch(event)) {
            x = event.touches[0].pageX - event.touches[0].target.offsetLeft;
            y = event.touches[0].pageY - event.touches[0].target.offsetTop;
        } else {
            x = event.layerX;
            y = event.layerY;
        }
        return {
            x: x,
            y: y
        };
    };

    this.onMouseDown = function (evt) {
        evt.preventDefault();
        b.flag = true;
        b.p = b.pos(evt);
        b.cx = b.p.x;
        b.cy = b.p.y;
        b.ctx.beginPath();
        b.ctx.arc(b.cx, b.cy, b.ctx.lineWidth / 2, 0, Math.PI * 2, true);
        b.ctx.closePath();
        b.ctx.fill();
        b.ctx.moveTo(b.p.x, b.p.y);
    };

    this.onMouseMove = function (evt) {
        evt.preventDefault();
        if (b.flag) {
            b.p = b.pos(evt);
            b.ctx.beginPath();
            b.ctx.moveTo(b.cx, b.cy);
            b.ctx.lineTo(b.p.x, b.p.y);
            b.ctx.closePath();
            b.ctx.stroke();
            b.cx = b.p.x;
            b.cy = b.p.y;
        }
    };

    this.onMouseUp = function (evt) {
        evt.preventDefault();
        b.flag = false;
        if (b.step.length < 11) {
            b.step.push(b.canvas.toDataURL('image/png'));
        }
    };
    this.canvas.addEventListener('mousedown', this.onMouseDown, false);
    this.canvas.addEventListener('mousemove', this.onMouseMove, false);
    this.canvas.addEventListener('mouseup', this.onMouseUp, false);
    this.canvas.addEventListener('touchstart', this.onMouseDown, false);
    this.canvas.addEventListener('touchmove', this.onMouseMove, false);
    this.canvas.addEventListener('touchend', this.onMouseUp, false);
}

$(function () {
    'use strict';
    var body_w = $('body').width(),
        body_h = $('.paint').height(),
        canvas_width = body_w - 20,
        canvas_height = body_h * 0.6;

    $('canvas').attr({
        width: canvas_width,
        height: canvas_height
    });

    var brush = new BrushDraw();

    $('.btn-redraw').click(function () {
        if (brush.step.length > 1) {
            brush.step.pop();
            var image = new Image();
            image.src = brush.step[brush.step.length - 1];
            brush.ctx.clearRect(0, 0, brush.canvas.width, brush.canvas.height);
            setTimeout(function () {
                brush.ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, brush.canvas.width, brush.canvas.height);
            }, 20);
        } else {
            brush.step.pop();
            brush.ctx.clearRect(0, 0, brush.canvas.width, brush.canvas.height);
        }
    });
    $('.btn-clear').click(function () {
        brush.step = [];
        brush.ctx.clearRect(0, 0, brush.canvas.width, brush.canvas.height);
    });
    $('.btn-save').click(function () {
        $('.share').fadeIn();
        var form,
            content = [],
            genId;
        content = '[{"image":"' + brush.canvas.toDataURL('image/png') + '"}]';
        form = {
            "key": "tuye_painter",
            "id": "userid",
            "content": content
        };
        $.ajax({
            url: "http://zhan.qianlong.com/weixin/painter_post",
            type: "post",
            data: form,
            dataType: "json",
            success: function (data) {
                genId = data.resultcode;
                location.hash = genId;
//                var _url = "http://comic.qianlong.com/zt/aerospace60th/get.html?id=" + genId;
                var _url = location.href;
                var obj_share = {
                    title: '面朝大海 不止于仰望星空',
                    desc: '我心中的未来航天器长这样！你呢？',
                    link: _url,
                    imgUrl: 'http://comic.qianlong.com/m/ico.jpg',
                    type: 'link',
                    dataUrl: '',
                    success: function () {
                        _hmt.push(['_trackEvent', 'hangtian', 'share_sent', 'weixin']);
                        setTimeout(function () {
                            window.location.href = _url;
                        }, 2000);
                    },
                    error: function (e) {
                        alert("数据上传失败，请检查网络重新尝试分享!");
                    }
                };
                wx.ready(function () {
                    wx.onMenuShareAppMessage(obj_share);
                    wx.onMenuShareQQ(obj_share);
                    wx.onMenuShareWeibo(obj_share);
                    wx.onMenuShareTimeline(obj_share);
                });
            }
        });
    });
});
